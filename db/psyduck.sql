/*
 Navicat Premium Data Transfer

 Source Server         : 本地5.7
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : localhost:3307
 Source Schema         : psyduck

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 26/04/2021 11:17:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for magic_api_file
-- ----------------------------
DROP TABLE IF EXISTS `magic_api_file`;
CREATE TABLE `magic_api_file`  (
  `file_path` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_content` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`file_path`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of magic_api_file
-- ----------------------------
INSERT INTO `magic_api_file` VALUES ('/magic-api/api/数组字典/', 'this is directory');
INSERT INTO `magic_api_file` VALUES ('/magic-api/api/数组字典/group.json', '{\r\n  \"id\" : \"8b1b9b1f9fa54493958186e283f29fb4\",\r\n  \"name\" : \"数组字典\",\r\n  \"type\" : \"1\",\r\n  \"parentId\" : \"0\",\r\n  \"path\" : \"xxx\",\r\n  \"paths\" : [ ],\r\n  \"options\" : [ ]\r\n}');
INSERT INTO `magic_api_file` VALUES ('/magic-api/api/数组字典/获取列表.ms', '{\r\n  \"id\" : \"48a9738818bc4b4f999efaf83d9e262d\",\r\n  \"script\" : null,\r\n  \"groupId\" : \"8b1b9b1f9fa54493958186e283f29fb4\",\r\n  \"name\" : \"获取列表\",\r\n  \"createTime\" : 1619406814063,\r\n  \"updateTime\" : 1619406814063,\r\n  \"method\" : \"GET\",\r\n  \"path\" : \"asdf\",\r\n  \"parameters\" : [ ],\r\n  \"option\" : \"[]\",\r\n  \"requestBody\" : \"\",\r\n  \"headers\" : [ ],\r\n  \"paths\" : [ ],\r\n  \"responseBody\" : null,\r\n  \"description\" : null,\r\n  \"optionMap\" : { }\r\n}\r\n================================\r\nreturn db.select(\"\"\"\r\n    select * from sys_dict\r\n\"\"\")');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `desc_ribe` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `dict_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '字典类型：0系统类，1业务类',
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '类型',
  `sort` int(5) NULL DEFAULT 0 COMMENT '排序',
  `is_del` int(1) NULL DEFAULT 0 COMMENT '删除标识：0未删除，1已删除',
  `create_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '字典表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1b3954d567a5457f94a539297d457b40', '3', '1', '1', 2, 1, NULL, NULL, NULL, NULL, '4');
INSERT INTO `sys_dict` VALUES ('3c393981-9ddd-40b2-8c19-85f0a9d9a98f', '组织机构类型', '0', 'office_type', 0, 0, '1', '2021-04-22 21:30:30', '1', '2021-04-22 21:30:30', '');
INSERT INTO `sys_dict` VALUES ('4e0a75e9c9124d29a5464cb635e18494', 's8', '1', 's8', 1, 1, NULL, NULL, NULL, NULL, 's8');
INSERT INTO `sys_dict` VALUES ('684fc15bf8694998891eca72bf1b11e9', '企业', '1', 'organ', 1, 1, NULL, NULL, NULL, NULL, '企业');
INSERT INTO `sys_dict` VALUES ('7368897911294388aded978c1763d0f6', '1', '1', '1', 0, 1, NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_dict` VALUES ('9177b87f926f44eda662f8390ad3b3f1', '企业状态', '1', 'organ1', 1, 1, NULL, NULL, NULL, NULL, '企业');
INSERT INTO `sys_dict` VALUES ('94a362e6d8214d93a9c9200ee326ecee', '3', '1', '1', 2, 1, NULL, NULL, NULL, NULL, '4');
INSERT INTO `sys_dict` VALUES ('95ec2cc79fc647efb941167113c3a089', '3', '1', '1', 2, 1, NULL, NULL, NULL, NULL, '4');
INSERT INTO `sys_dict` VALUES ('ae9a2cd400264ff6bdc2f00b62d6e911', '字典类型', '0', 'dict_type', 0, 0, NULL, NULL, '2', '2021-03-17 19:36:10', NULL);
INSERT INTO `sys_dict` VALUES ('ae9a2cd400264ff6bdc2f00b62d6e941', '是否登录', '0', 'is_login', 1, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES ('b1c13a6b80dd4a0d9f7344c9e219265f', '1', '1', '1', 0, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES ('ce64ab1e9d0c45f7a8ba3f218321981d', '3', '1', '1', 2, 1, NULL, NULL, NULL, NULL, '4');
INSERT INTO `sys_dict` VALUES ('cedc9783ce89443e91060245c4a4f610', '1', '1', '1', 0, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES ('da5f0f7da9b14a16b45044bc367e3b68', '3', '1', '1', 2, 1, NULL, NULL, NULL, NULL, '4');
INSERT INTO `sys_dict` VALUES ('eaa474f9c32e4bf49999876f242b64d8', '1', '1', '1', 0, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES ('f1c3544061b8489587068ca80fa8ceae', '1', '1', '1', 0, 1, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_items
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_items`;
CREATE TABLE `sys_dict_items`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '值',
  `label` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '标签',
  `dict_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'sys_dict表id',
  `sort` int(5) NOT NULL DEFAULT 0 COMMENT '排序',
  `is_del` int(1) NULL DEFAULT 0 COMMENT '删除标识：0未删除，1已删除',
  `create_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '字典项' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_items
-- ----------------------------
INSERT INTO `sys_dict_items` VALUES ('0f91de6e8406d59762bebe5d2dfc9d36', '0', '不禁用', 'ae9a2cd400264ff6bdc2f00b62d6e941', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_items` VALUES ('c5cf8715a74537156ea29c8bbc622b05', '1', '禁用', 'ae9a2cd400264ff6bdc2f00b62d6e941', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_items` VALUES ('ze9a2cd400264ff6bdc2f00b62d6e911', '0', '系统类', 'ae9a2cd400264ff6bdc2f00b62d6e911', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_items` VALUES ('ze9a2cd400264ff6bdc2f00b62d6e910', '1', '业务类', 'ae9a2cd400264ff6bdc2f00b62d6e911', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_items` VALUES ('42792febef2a468abe6dad4275c8dd2a', '1', '1', '4e0a75e9c9124d29a5464cb635e18494', 0, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_items` VALUES ('bb6b8eb9e6f94583bd78b71f6536aa67', '2', '2', '4e0a75e9c9124d29a5464cb635e18494', 0, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_items` VALUES ('4a24449a46174bba81bc25c4555b76f1', '1', '未开通', '9177b87f926f44eda662f8390ad3b3f1', 0, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_items` VALUES ('85f1b884937e4cdba6d3ede1c73ad09f', '2', '已开通', '9177b87f926f44eda662f8390ad3b3f1', 0, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_items` VALUES ('a8a973db35a64fa7b29a350cda23f9f9', '2', '1', '1b3954d567a5457f94a539297d457b40', 3, 1, NULL, NULL, NULL, NULL, '4');
INSERT INTO `sys_dict_items` VALUES ('a3fbeea983864a8eb870e23d53e51dba', '3', '4', '1b3954d567a5457f94a539297d457b40', 2, 1, NULL, NULL, NULL, NULL, '1');
INSERT INTO `sys_dict_items` VALUES ('22a52184dde447308238998e65ba09be', '2', '1', '1b3954d567a5457f94a539297d457b40', 3, 1, NULL, NULL, NULL, NULL, '4');
INSERT INTO `sys_dict_items` VALUES ('b928f5b1-07bd-4f09-9142-897c767c5303', '1', '部门', '3c393981-9ddd-40b2-8c19-85f0a9d9a98f', 0, 0, '1', '2021-04-22 21:31:18', '1', '2021-04-22 21:31:18', '');
INSERT INTO `sys_dict_items` VALUES ('457517be-5245-4d98-b9fa-8b6753a9cfc6', '2', '公司', '3c393981-9ddd-40b2-8c19-85f0a9d9a98f', 0, 0, '1', '2021-04-22 21:31:25', '1', '2021-04-22 21:31:25', '');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '文件相对路径',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `external_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '外部id，其他表数据id',
  `external_type` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '外部文件类型，比如：是营业执照啊 还是 头像啊',
  `is_del` int(1) NULL DEFAULT 0 COMMENT '删除标识',
  `create_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '文件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_file
-- ----------------------------
INSERT INTO `sys_file` VALUES ('2ab48e59-bddc-4fa4-9591-e99972c0fad9', 'userfiles/2021-04-25/0c710e36737f4f969894559d8f01f3ab.jpg', 5, '783fc7b4f5354c98be2b054fb17ebb06', '营业执照', 0, '1', '2021-04-25 00:18:24', '1', '2021-04-25 00:18:24');
INSERT INTO `sys_file` VALUES ('565bab99-60dc-49a3-8137-f88835a7ad28', 'userfiles/2021-04-25/a5bfdd92f69a497ead33ac9a9167f703.jpg', 3, '783fc7b4f5354c98be2b054fb17ebb06', '营业执照', 0, '1', '2021-04-25 00:18:24', '1', '2021-04-25 00:18:24');
INSERT INTO `sys_file` VALUES ('885decd9-97ec-4bbf-bae5-54e9aff0c566', 'userfiles/2021-04-25/153a3157abef416b8bbadd5d7285035c.jpg', 1, '54f7a599b5524b4eaabcb1bf6259fd98', '营业执照', 0, '1', '2021-04-25 09:34:19', '1', '2021-04-25 09:34:19');
INSERT INTO `sys_file` VALUES ('adf18fe3-f047-4422-993f-099447c4b29a', 'userfiles/2021-04-25/2cfccc4e11e1478b906fd071b69671b1.jpg', 4, '783fc7b4f5354c98be2b054fb17ebb06', '营业执照', 0, '1', '2021-04-25 00:18:24', '1', '2021-04-25 00:18:24');
INSERT INTO `sys_file` VALUES ('ae94dbe5-dc08-4fd5-a7fe-d22e529b74f5', 'userfiles/2021-04-25/cea72ece5a6e489ea4f5873197f562ed.jpg', 2, '54f7a599b5524b4eaabcb1bf6259fd98', '营业执照', 0, '1', '2021-04-25 09:34:19', '1', '2021-04-25 09:34:19');
INSERT INTO `sys_file` VALUES ('c48aef35-08b2-4a02-836a-3e78b7f9615e', 'userfiles/2021-04-25/bd1fb99b5170409790916df7074dcbc3.jpg', 1, '783fc7b4f5354c98be2b054fb17ebb06', '营业执照', 0, '1', '2021-04-25 00:18:24', '1', '2021-04-25 00:18:24');
INSERT INTO `sys_file` VALUES ('e7f97a6e-4eef-4bce-8a50-919cc871d674', 'userfiles/2021-04-25/16e0f8bacc2945dea56b0b2deb3322a1.jpg', 2, '783fc7b4f5354c98be2b054fb17ebb06', '营业执照', 0, '1', '2021-04-25 00:18:24', '1', '2021-04-25 00:18:24');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pid` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `icon` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '图标（base64）',
  `desc_ribe` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '菜单链接',
  `permission` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '权限',
  `sort` int(12) NULL DEFAULT NULL,
  `is_list_config` int(1) NULL DEFAULT NULL COMMENT '是否开启list配置：0开启，1不开启',
  `list_config` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '列表配置json',
  `form_config` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '表单配置json',
  `is_del` int(1) NULL DEFAULT 0,
  `create_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  `is_show` int(1) NULL DEFAULT NULL COMMENT '是否显示，1显示，0不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '菜单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('0048ee96-436c-46a0-a002-b1a7a4a97663', '641253af-8ea1-4b5d-8bc3-a7165ef60ff2', NULL, '', '1', '', '', 0, 1, NULL, NULL, 1, '1', '2021-04-11 16:20:54', '1', '2021-04-11 16:20:54', 1);
INSERT INTO `sys_menu` VALUES ('06b8a427e4cd4c1ba11752070f565f20', 'b1851d1b13594e71840103c11a37a669', NULL, '', '用户管理', '/system/user/user-list', '', 5, NULL, NULL, NULL, 0, NULL, NULL, '1', '2021-03-05 21:48:24', 1);
INSERT INTO `sys_menu` VALUES ('0dd29ec8-8767-4155-9c83-111c50d83322', '641253af-8ea1-4b5d-8bc3-a7165ef60ff2', NULL, '', '10', '', '', 0, 1, NULL, NULL, 1, '1', '2021-04-11 16:21:54', '1', '2021-04-11 16:21:54', 1);
INSERT INTO `sys_menu` VALUES ('24176912aa9e4b2d832f96292be3430e', '81b6845a20da4028a01a42f3eb0b4f4b', NULL, '', '删除', '', 'dict:items:delete', 0, NULL, NULL, NULL, 0, '1', '2020-12-10 10:38:43', '1', '2020-12-10 10:38:43', 0);
INSERT INTO `sys_menu` VALUES ('342f7951-3a06-499f-8fd9-645e9fc42089', '641253af-8ea1-4b5d-8bc3-a7165ef60ff2', NULL, '', '8', '', '', 0, 1, NULL, NULL, 1, '1', '2021-04-11 16:21:48', '1', '2021-04-11 16:21:48', 1);
INSERT INTO `sys_menu` VALUES ('36c626aa-9d8d-4556-992f-03c53ce9b584', '641253af-8ea1-4b5d-8bc3-a7165ef60ff2', NULL, '', '2', '', '', 0, 1, NULL, NULL, 1, '1', '2021-04-11 16:20:58', '1', '2021-04-11 16:20:58', 1);
INSERT INTO `sys_menu` VALUES ('36cef324-7d0a-4104-a820-4566ade2284b', 'da95db9f-5821-4da6-b524-92f9095a649d', NULL, '5', '1', '2', '3', 4, 1, NULL, NULL, 1, '1', '2021-04-22 12:24:22', '1', '2021-04-22 12:24:26', 1);
INSERT INTO `sys_menu` VALUES ('39be13ef6f0745568c80bf35202ddb2b', 'b1851d1b13594e71840103c11a37a669', NULL, '', '菜单管理', '/system/menu/menu-list', '', 1, NULL, NULL, NULL, 0, NULL, NULL, '1', '2021-03-05 21:49:48', 1);
INSERT INTO `sys_menu` VALUES ('414e5d31-fe05-4e69-9983-217c10b9740a', '39be13ef6f0745568c80bf35202ddb2b', NULL, '', '权限测试', '', 'sys:test:aaa', 0, 1, NULL, NULL, 0, '1', '2021-03-25 20:40:12', '1', '2021-03-25 20:40:21', 0);
INSERT INTO `sys_menu` VALUES ('4526eb9c-4b7a-4945-bfa9-cddc01f36c22', '6f3594d0-5445-41e1-a13c-890a57485036', NULL, '', '保存', '', 'office:office:save', 0, 1, NULL, NULL, 0, '1', '2021-04-22 20:43:09', '1', '2021-04-22 20:43:09', 0);
INSERT INTO `sys_menu` VALUES ('4cfac38d-dcec-4c11-aff4-12e03779f1e4', 'b1851d1b13594e71840103c11a37a669', NULL, '', 'test', 'test/test', '', 0, NULL, NULL, NULL, 1, '1', '2021-03-21 15:34:41', '1', '2021-03-21 15:34:41', 1);
INSERT INTO `sys_menu` VALUES ('546c874267514672844cd3a019e468f7', 'c5f407478c4e4c9cbcdbee6389d2c909', NULL, '', '删除', '', 'dict:delete', 0, NULL, NULL, NULL, 0, '1', '2020-11-11 14:11:38', '1', '2020-11-12 00:25:31', 0);
INSERT INTO `sys_menu` VALUES ('641253af-8ea1-4b5d-8bc3-a7165ef60ff2', '82de38c53fbe4c4185e31d972d8c2d5e', NULL, '', '一些例子', '/examples', '', 0, 1, NULL, NULL, 0, '1', '2021-03-28 22:59:56', '1', '2021-03-28 23:03:52', 1);
INSERT INTO `sys_menu` VALUES ('6762231e-4f30-4a6b-a94e-b185e99ed608', '6f3594d0-5445-41e1-a13c-890a57485036', NULL, '', '删除', '', 'office:office:delete', 0, 1, NULL, NULL, 0, '1', '2021-04-22 20:42:56', '1', '2021-04-22 20:42:56', 0);
INSERT INTO `sys_menu` VALUES ('684723f4226948aba1661292961c8f71', '8e9455740091486c914495cfb0c7faa5', NULL, '', '删除', '', 'role:delete', 0, NULL, NULL, NULL, 0, '1', '2020-11-11 13:59:45', '1', '2020-11-12 07:48:14', 0);
INSERT INTO `sys_menu` VALUES ('6f3594d0-5445-41e1-a13c-890a57485036', 'b1851d1b13594e71840103c11a37a669', NULL, '', '组织机构', '/system/office/office-list', 'office:office:list', 2, 1, NULL, NULL, 0, '1', '2021-04-22 20:40:14', '1', '2021-04-22 20:42:44', 1);
INSERT INTO `sys_menu` VALUES ('6fe87749-fdb1-48a0-bdbb-c855ff64424d', 'da95db9f-5821-4da6-b524-92f9095a649d', NULL, '', '1', '', '', 0, 1, NULL, NULL, 1, '1', '2021-04-22 11:51:00', '1', '2021-04-22 11:51:00', 1);
INSERT INTO `sys_menu` VALUES ('7bc33c6a-2f5d-4d2e-af2e-5c73689731e1', '641253af-8ea1-4b5d-8bc3-a7165ef60ff2', NULL, '', '5', '', '', 0, 1, NULL, NULL, 1, '1', '2021-04-11 16:21:09', '1', '2021-04-11 16:21:09', 1);
INSERT INTO `sys_menu` VALUES ('7d3f4a76-d73b-4d49-bc51-99105d42ca38', '641253af-8ea1-4b5d-8bc3-a7165ef60ff2', NULL, '', '9', '', '', 0, 1, NULL, NULL, 1, '1', '2021-04-11 16:21:51', '1', '2021-04-11 16:21:51', 1);
INSERT INTO `sys_menu` VALUES ('8009eed4f46141e3aca939490d68786a', 'b1851d1b13594e71840103c11a37a669', NULL, '', '区域管理', '/system/area', '', 3, NULL, NULL, NULL, 0, '1', '2019-11-15 10:23:07', '1', '2020-12-09 16:19:35', 0);
INSERT INTO `sys_menu` VALUES ('81b6845a20da4028a01a42f3eb0b4f4b', 'c5f407478c4e4c9cbcdbee6389d2c909', NULL, '', '字典项', '', 'dict:items', 0, NULL, NULL, NULL, 0, '1', '2020-12-10 09:35:02', '1', '2020-12-10 10:39:06', 0);
INSERT INTO `sys_menu` VALUES ('82de38c53fbe4c4185e31d972d8c2d5e', '', NULL, NULL, '功能菜单', '', '', 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES ('833b204d5f2c402190bfca677421cfeb', '39be13ef6f0745568c80bf35202ddb2b', NULL, '', '删除', '', 'menu:delete', 2, NULL, NULL, NULL, 0, '1', '2020-11-11 11:06:05', '1', '2020-11-11 13:58:55', 0);
INSERT INTO `sys_menu` VALUES ('8c653d00-12c3-479e-8b23-fa05abf97663', '641253af-8ea1-4b5d-8bc3-a7165ef60ff2', NULL, '', '7', '', '', 0, 1, NULL, NULL, 1, '1', '2021-04-11 16:21:44', '1', '2021-04-11 16:21:44', 1);
INSERT INTO `sys_menu` VALUES ('8e9455740091486c914495cfb0c7faa5', 'b1851d1b13594e71840103c11a37a669', NULL, '', '角色管理', '/system/role/role-list', 'role:view', 4, NULL, NULL, NULL, 0, NULL, NULL, '1', '2021-03-05 21:50:05', 1);
INSERT INTO `sys_menu` VALUES ('94071254597d4da286bb8112ad93f4ff', '8e9455740091486c914495cfb0c7faa5', NULL, '', '保存', '', 'role:save', 0, NULL, NULL, NULL, 0, '1', '2020-11-11 13:59:27', '1', '2020-11-11 13:59:27', 0);
INSERT INTO `sys_menu` VALUES ('aa08fab6-1468-4450-b625-be50a132d184', 'da95db9f-5821-4da6-b524-92f9095a649d', NULL, '', '1', '', '', 0, 1, NULL, NULL, 1, '1', '2021-04-22 11:54:31', '1', '2021-04-22 11:54:31', 1);
INSERT INTO `sys_menu` VALUES ('b1851d1b13594e71840103c11a37a669', '82de38c53fbe4c4185e31d972d8c2d5e', NULL, NULL, '系统设置', '/system', '', 0, NULL, NULL, NULL, 0, NULL, NULL, '1', '2020-12-09 16:19:05', 1);
INSERT INTO `sys_menu` VALUES ('b353de07f3624b4fa1c6f8b1b1e80cad', 'c5f407478c4e4c9cbcdbee6389d2c909', NULL, '', '保存', '', 'dict:save', 0, NULL, NULL, NULL, 0, '1', '2020-11-11 14:11:28', '1', '2020-11-12 00:25:32', 0);
INSERT INTO `sys_menu` VALUES ('bcd261d6-06c0-4e88-9b33-b6bb208bbe86', '641253af-8ea1-4b5d-8bc3-a7165ef60ff2', NULL, '', '3', '', '', 0, 1, NULL, NULL, 1, '1', '2021-04-11 16:21:02', '1', '2021-04-11 16:21:02', 1);
INSERT INTO `sys_menu` VALUES ('bd7a7133-67c1-49cb-b1c4-f5c6dcb46796', '641253af-8ea1-4b5d-8bc3-a7165ef60ff2', NULL, '', '6', '', '', 0, 1, NULL, NULL, 1, '1', '2021-04-11 16:21:13', '1', '2021-04-11 16:21:13', 1);
INSERT INTO `sys_menu` VALUES ('bee5c6cb1b484133a4b8e72fe5c5eed0', '06b8a427e4cd4c1ba11752070f565f20', NULL, '', '保存', '', 'user:save', 0, NULL, NULL, NULL, 0, '1', '2020-11-11 13:27:06', '1', '2020-11-11 13:27:06', 0);
INSERT INTO `sys_menu` VALUES ('c1c4f05d-5759-4531-b60a-338a9dba365c', 'da95db9f-5821-4da6-b524-92f9095a649d', NULL, '4', '12', '1', '2', 3, 1, NULL, NULL, 1, '1', '2021-04-22 12:01:44', '1', '2021-04-22 12:01:50', 1);
INSERT INTO `sys_menu` VALUES ('c5f407478c4e4c9cbcdbee6389d2c909', 'b1851d1b13594e71840103c11a37a669', NULL, '', '数据字典', '/system/dict/dict-list', 'dict:view', 6, NULL, NULL, NULL, 0, '1', '2019-11-15 10:22:28', '1', '2021-03-05 21:50:37', 1);
INSERT INTO `sys_menu` VALUES ('c6e7b5866ce64bfdabeb5dd6342ca55f', '06b8a427e4cd4c1ba11752070f565f20', NULL, '', '删除', '', 'user:delete', 0, NULL, NULL, NULL, 0, '1', '2020-11-11 13:27:22', '1', '2020-11-11 13:27:22', 0);
INSERT INTO `sys_menu` VALUES ('d7e5280a2a8f4fb3b9e2f4a3e3093a38', '39be13ef6f0745568c80bf35202ddb2b', NULL, '', '保存', '', 'menu:save', 1, NULL, NULL, NULL, 0, '1', '2020-11-11 13:58:32', '1', '2020-11-11 23:06:31', 0);
INSERT INTO `sys_menu` VALUES ('da801842-9d64-4d3c-a912-732d47373061', 'b1851d1b13594e71840103c11a37a669', NULL, '', '接口管理', '/magic/magic-api', '', 0, 1, NULL, NULL, 0, '1', '2021-03-13 22:44:56', '1', '2021-04-11 16:09:13', 1);
INSERT INTO `sys_menu` VALUES ('da95db9f-5821-4da6-b524-92f9095a649d', 'b1851d1b13594e71840103c11a37a669', NULL, '', 'test', 'test', '', 0, 0, '{\"queryOptions\":{\"btns\":[{\"type\":\"query\"},{\"type\":\"export\",\"headers\":[\"登录名称\",\"姓名\"],\"columns\":[\"username\",\"name\"]}],\"cols\":[{\"field\":\"username\",\"type\":\"input\",\"el\":{\"type\":\"text\"},\"title\":\"登录名称\"},{\"type\":\"dict\",\"dictType\":\"is_login\",\"field\":\"isLogin\",\"title\":\"是否禁用\"}]},\"toolOptions\":{\"btns\":[{\"btnType\":\"add\"},{\"btnType\":\"delete\",\"request\":{\"url\":\"user/delete\",\"data\":{\"id\":\"rowField\",\"status\":0}}}]},\"tableOptions\":{\"url\":\"user/list\",\"page\":true,\"selection\":true,\"cols\":[{\"field\":\"username\",\"title\":\"登录名称\"},{\"field\":\"name\",\"title\":\"姓名/昵称\"},{\"field\":\"phone\",\"title\":\"手机号\"},{\"type\":\"switch\",\"field\":\"isLogin\",\"title\":\"禁止登录\",\"request\":{\"url\":\"user/changeIsLogin\",\"method\":\"get\",\"data\":{\"id\":\"rowField\",\"isLogin\":\"rowField\"}}},{\"type\":\"btns\",\"title\":\"操作\",\"width\":200,\"btns\":[{\"title\":\"修改\",\"permission\":\"user:save\",\"btnType\":\"update\",\"type\":\"primary\",\"request\":{\"url\":\"user/save\"}},{\"title\":\"删除\",\"permission\":\"user:delete\",\"btnType\":\"delete\",\"type\":\"danger\",\"request\":{\"url\":\"user/delete\"}}]}]}}', '{\"detail\":{\"request\":{\"url\":\"user/detail\"},\"excludeAssign\":[\"password\"]},\"form\":{\"request\":{\"url\":\"user/save\",\"method\":\"post\"},\"style\":\"width: 400px; margin-left:50px;\",\"rows\":[{\"gutter\":20,\"cols\":[{\"span\":20,\"label\":\"登录名称\",\"type\":\"input\",\"name\":\"username\",\"rule\":[{\"required\":true,\"message\":\"请输入登录名称\",\"trigger\":\"change\"}]},{\"span\":20,\"label\":\"姓名/昵称\",\"type\":\"input\",\"name\":\"name\"},{\"span\":20,\"label\":\"密码\",\"type\":\"password\",\"name\":\"password\"},{\"span\":20,\"label\":\"手机号\",\"type\":\"input\",\"name\":\"phone\"},{\"span\":20,\"label\":\"禁止登陆\",\"type\":\"switch\",\"activeValue\":1,\"inactiveValue\":0,\"name\":\"isLogin\"},{\"span\":20,\"label\":\"选择角色\",\"type\":\"checkboxGroup\",\"defaultValue\":{\"text\":\"name\",\"value\":\"id\",\"request\":{\"url\":\"role/getRoles\",\"method\":\"get\"}},\"name\":\"roles\"}]}]}}', 0, '1', '2021-03-21 17:33:27', '1', '2021-04-22 12:24:43', 1);
INSERT INTO `sys_menu` VALUES ('dc332875-831e-4937-86ff-0c7420915ce9', '641253af-8ea1-4b5d-8bc3-a7165ef60ff2', NULL, '', '三级联动', '/examples/three-linkage', '', 0, 1, NULL, NULL, 0, '1', '2021-03-28 23:04:14', '1', '2021-03-28 23:04:17', 1);
INSERT INTO `sys_menu` VALUES ('e46a01397a01459bb478a5f31a6220ee', '81b6845a20da4028a01a42f3eb0b4f4b', NULL, '', '保存', '', 'dict:items:save', 0, NULL, NULL, NULL, 0, '1', '2020-12-10 10:38:27', '1', '2020-12-10 10:38:27', 0);
INSERT INTO `sys_menu` VALUES ('e8dc8a7e-263d-4b65-9f0d-080bc11ec223', '641253af-8ea1-4b5d-8bc3-a7165ef60ff2', NULL, '', '4', '', '', 0, 1, NULL, NULL, 1, '1', '2021-04-11 16:21:06', '1', '2021-04-11 16:21:06', 1);
INSERT INTO `sys_menu` VALUES ('f09410d5-5043-4164-9f68-8d135de71b29', '641253af-8ea1-4b5d-8bc3-a7165ef60ff2', NULL, '', '上传图片', '/examples/upload-image', '', 0, 1, NULL, NULL, 0, '1', '2021-04-23 22:52:04', '1', '2021-04-23 22:52:04', 1);

-- ----------------------------
-- Table structure for sys_office
-- ----------------------------
DROP TABLE IF EXISTS `sys_office`;
CREATE TABLE `sys_office`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '名称',
  `type` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '类型',
  `pid` varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '父id',
  `sort` int(12) NULL DEFAULT NULL COMMENT '排序',
  `is_del` int(1) NULL DEFAULT 0 COMMENT '删除标识',
  `create_by` varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_permission_code
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission_code`;
CREATE TABLE `sys_permission_code`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主键',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '权限代码',
  `pid` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '父id',
  `is_del` int(1) NULL DEFAULT 0 COMMENT '删除标识：0未删除，1已删除',
  `create_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '角色名称',
  `sort` int(12) NULL DEFAULT NULL COMMENT '排序',
  `is_del` int(1) NULL DEFAULT 0,
  `create_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '1', '管理员', NULL, 0, NULL, NULL, '1382275268005437442', '2021-04-14 18:13:16');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '角色id',
  `menu_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '菜单id'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1', 'b1851d1b13594e71840103c11a37a669');
INSERT INTO `sys_role_menu` VALUES ('1', 'da801842-9d64-4d3c-a912-732d47373061');
INSERT INTO `sys_role_menu` VALUES ('1', '39be13ef6f0745568c80bf35202ddb2b');
INSERT INTO `sys_role_menu` VALUES ('1', 'd7e5280a2a8f4fb3b9e2f4a3e3093a38');
INSERT INTO `sys_role_menu` VALUES ('1', '833b204d5f2c402190bfca677421cfeb');
INSERT INTO `sys_role_menu` VALUES ('1', '8009eed4f46141e3aca939490d68786a');
INSERT INTO `sys_role_menu` VALUES ('1', '8e9455740091486c914495cfb0c7faa5');
INSERT INTO `sys_role_menu` VALUES ('1', '684723f4226948aba1661292961c8f71');
INSERT INTO `sys_role_menu` VALUES ('1', '94071254597d4da286bb8112ad93f4ff');
INSERT INTO `sys_role_menu` VALUES ('1', '06b8a427e4cd4c1ba11752070f565f20');
INSERT INTO `sys_role_menu` VALUES ('1', 'bee5c6cb1b484133a4b8e72fe5c5eed0');
INSERT INTO `sys_role_menu` VALUES ('1', 'c6e7b5866ce64bfdabeb5dd6342ca55f');
INSERT INTO `sys_role_menu` VALUES ('1', 'c5f407478c4e4c9cbcdbee6389d2c909');
INSERT INTO `sys_role_menu` VALUES ('1', '546c874267514672844cd3a019e468f7');
INSERT INTO `sys_role_menu` VALUES ('1', '81b6845a20da4028a01a42f3eb0b4f4b');
INSERT INTO `sys_role_menu` VALUES ('1', '24176912aa9e4b2d832f96292be3430e');
INSERT INTO `sys_role_menu` VALUES ('1', 'e46a01397a01459bb478a5f31a6220ee');
INSERT INTO `sys_role_menu` VALUES ('1', 'b353de07f3624b4fa1c6f8b1b1e80cad');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '登录名',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '姓名/昵称',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '手机号',
  `is_login` int(1) NULL DEFAULT 0 COMMENT '禁止登录：0未禁用，1已禁用',
  `sort` int(12) NULL DEFAULT NULL COMMENT '排序',
  `is_del` int(1) NULL DEFAULT 0 COMMENT '删除标识：0未删除，1已删除',
  `create_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '管理员', '$2a$10$yCWHUEGVmJ8pOTTrfvb3r.bAQEeSXz6YPkjEVxGViQjf8rFVf.nNG', '1', '13888888888', 0, NULL, 0, NULL, '2020-05-15 22:25:20', '352203037d942140926eb0ea10a04a56', '2021-01-11 16:55:44');
INSERT INTO `sys_user` VALUES ('1367346377654542338', 'test_peizhi', 'test_peizhi', '', NULL, '', 1, NULL, 0, '1', '2021-03-04 13:29:15', '1', '2021-04-07 10:17:58');
INSERT INTO `sys_user` VALUES ('1367443354346070018', '测试删除', '1', '$2a$10$vBguJEJd.m2oOAhGe38S8OWaTnRrabKxxbBfpJpcDfYrlGzxL3XW2', NULL, '1', 0, NULL, 1, '1', '2021-03-04 19:54:36', '1', '2021-03-04 19:54:36');
INSERT INTO `sys_user` VALUES ('1367450023847272450', 'test1', '', '', NULL, '', 0, NULL, 0, '1', '2021-03-04 20:21:06', '1', '2021-03-04 20:21:06');
INSERT INTO `sys_user` VALUES ('1367450045733150721', 'test2', '', '', NULL, '', 0, NULL, 0, '1', '2021-03-04 20:21:12', '1', '2021-03-04 20:21:12');
INSERT INTO `sys_user` VALUES ('1367450056533483522', 'test3', '', '', NULL, '', 0, NULL, 0, '1', '2021-03-04 20:21:14', '1', '2021-03-04 20:21:14');
INSERT INTO `sys_user` VALUES ('1367450067145072642', 'test4', '', '', NULL, '', 0, NULL, 0, '1', '2021-03-04 20:21:17', '1', '2021-03-04 20:21:17');
INSERT INTO `sys_user` VALUES ('1367450080122249217', 'test5', '', '', NULL, '', 0, NULL, 0, '1', '2021-03-04 20:21:20', '1', '2021-03-04 20:21:20');
INSERT INTO `sys_user` VALUES ('1367450104713453569', 'test6', '', '', NULL, '', 0, NULL, 0, '1', '2021-03-04 20:21:26', '1', '2021-03-04 20:21:26');
INSERT INTO `sys_user` VALUES ('1367450118042951681', 'test7', '', '', NULL, '', 0, NULL, 0, '1', '2021-03-04 20:21:29', '1', '2021-03-04 20:21:29');
INSERT INTO `sys_user` VALUES ('1367450130332262402', 'test8', '', '', NULL, '', 0, NULL, 0, '1', '2021-03-04 20:21:32', '1', '2021-03-04 20:21:32');
INSERT INTO `sys_user` VALUES ('1367450144974577666', 'test9', '', '', NULL, '', 0, NULL, 0, '1', '2021-03-04 20:21:35', '1', '2021-03-04 20:21:35');
INSERT INTO `sys_user` VALUES ('1367450187030863874', 'test10', '', '', NULL, '', 0, NULL, 0, '1', '2021-03-04 20:21:45', '1', '2021-03-04 20:21:45');
INSERT INTO `sys_user` VALUES ('1367461975311011841', 'test10', '', '', NULL, '', 0, NULL, 0, '1', '2021-03-04 21:08:36', '1', '2021-03-04 21:08:36');
INSERT INTO `sys_user` VALUES ('1367468668598112258', 'test10', '', '', NULL, '', 0, NULL, 0, '1', '2021-03-04 21:35:12', '1', '2021-03-04 21:35:12');
INSERT INTO `sys_user` VALUES ('1367468695546515458', 'test10', '', '', NULL, '', 0, NULL, 0, '1', '2021-03-04 21:35:18', '1', '2021-03-04 21:35:18');
INSERT INTO `sys_user` VALUES ('1367468702639083522', 'test10', '', '', NULL, '', 0, NULL, 0, '1', '2021-03-04 21:35:20', '1', '2021-03-04 21:35:20');
INSERT INTO `sys_user` VALUES ('1367485930394636290', 'test', '', '', NULL, '', 0, NULL, 0, '1', '2021-03-04 22:43:47', '1', '2021-03-04 22:43:47');
INSERT INTO `sys_user` VALUES ('1367486269759967234', 'test1', '', '', NULL, '', 0, NULL, 0, '1', '2021-03-04 22:45:08', '1', '2021-03-04 22:45:08');
INSERT INTO `sys_user` VALUES ('1367486280220561410', 'test2', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-04 22:45:11', '1', '2021-03-04 22:45:11');
INSERT INTO `sys_user` VALUES ('1367486289682911234', 'test3', '', '', NULL, '', 0, NULL, 0, '1', '2021-03-04 22:45:13', '1', '2021-03-04 22:45:13');
INSERT INTO `sys_user` VALUES ('1367486302601367554', 'test4', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-04 22:45:16', '1', '2021-03-04 22:45:16');
INSERT INTO `sys_user` VALUES ('1367486316111220738', 'test5', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-04 22:45:19', '1', '2021-03-04 22:45:19');
INSERT INTO `sys_user` VALUES ('1367486329784651777', 'test6', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-04 22:45:22', '1', '2021-03-04 22:45:22');
INSERT INTO `sys_user` VALUES ('1367754288398622722', '1', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-05 16:30:09', '1', '2021-03-05 16:30:09');
INSERT INTO `sys_user` VALUES ('1367754752078929922', '1', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-05 16:31:59', '1', '2021-03-05 16:31:59');
INSERT INTO `sys_user` VALUES ('1367755254447497218', '12', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-05 16:33:59', '1', '2021-03-05 16:33:59');
INSERT INTO `sys_user` VALUES ('1367755655083220994', '131313', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-05 16:35:35', '1', '2021-03-05 16:35:35');
INSERT INTO `sys_user` VALUES ('1367755839821340673', 'asdfasdf', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-05 16:36:19', '1', '2021-03-05 16:36:19');
INSERT INTO `sys_user` VALUES ('1367755850953023490', 'asdfasdfasdfasdf', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-05 16:36:21', '1', '2021-03-05 16:36:21');
INSERT INTO `sys_user` VALUES ('1367755864114749442', 'asdfasdfasdfasdfxzcv', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-05 16:36:24', '1', '2021-03-05 16:36:24');
INSERT INTO `sys_user` VALUES ('1367756588886286337', 'asdf', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-05 16:39:17', '1', '2021-03-05 16:39:17');
INSERT INTO `sys_user` VALUES ('1367757761529798657', '1', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-05 16:43:57', '1', '2021-03-05 16:43:57');
INSERT INTO `sys_user` VALUES ('1367757893885255682', '1', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-05 16:44:28', '1', '2021-03-05 16:44:28');
INSERT INTO `sys_user` VALUES ('1367757905251819522', '3', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-05 16:44:31', '1', '2021-03-05 16:44:31');
INSERT INTO `sys_user` VALUES ('1367757991138582529', '1', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-05 16:44:51', '1', '2021-03-05 16:44:51');
INSERT INTO `sys_user` VALUES ('1371109623041716226', '1', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-14 22:43:03', '1', '2021-03-14 22:43:03');
INSERT INTO `sys_user` VALUES ('1371110380587569154', '1212', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-14 22:46:03', '1', '2021-03-14 22:46:03');
INSERT INTO `sys_user` VALUES ('1371110429463793665', '1', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-14 22:46:15', '1', '2021-03-14 22:46:15');
INSERT INTO `sys_user` VALUES ('1371110466939899906', '1', '', '', NULL, '', 0, NULL, 1, '1', '2021-03-14 22:46:24', '1', '2021-03-14 22:46:24');
INSERT INTO `sys_user` VALUES ('1379778439112048642', 'ljztest', '吕金泽测试', '', NULL, '13888888888', 1, NULL, 1, '1', '2021-04-07 20:49:50', '1', '2021-04-07 20:55:32');
INSERT INTO `sys_user` VALUES ('1382275268005437442', 'ljz', '', '$2a$10$BviuGIqRHHKdJuX7pbpqauULFjsBUPEyl6c7YWXsyxE63DzxkoCKO', NULL, '', 0, NULL, 0, '1', '2021-04-14 18:11:20', '1', '2021-04-14 18:11:20');
INSERT INTO `sys_user` VALUES ('cdb5ea555c884f5e95b9625bc19ac93c', 'ljz1', '吕金泽23', '$2a$10$8m9QOeSRTwU/GQleRzitJekI/Ji8XBt15w6kzkupjYe3KqMRvz3fW', NULL, '138111111', 0, NULL, 1, NULL, '2021-04-05 15:07:41', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_code
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_code`;
CREATE TABLE `sys_user_code`  (
  `user_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户id',
  `code_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '权限代码id',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user_office
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_office`;
CREATE TABLE `sys_user_office`  (
  `office_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '用户id',
  `role_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '角色id'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户与角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('1367346377654542338', '');
INSERT INTO `sys_user_role` VALUES ('1367346377654542338', '1');
INSERT INTO `sys_user_role` VALUES ('1379778439112048642', '1');
INSERT INTO `sys_user_role` VALUES ('1382275268005437442', '1');

SET FOREIGN_KEY_CHECKS = 1;
