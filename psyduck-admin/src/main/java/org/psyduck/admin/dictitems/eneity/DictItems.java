package org.psyduck.admin.dictitems.eneity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.psyduck.common.base.BaseEntity;

@Data
@TableName("sys_dict_items")
public class DictItems extends BaseEntity {

    private String value;

    private String label;

    private String dictId;

    private Integer sort;

    private String remarks;

}
