package org.psyduck.admin.dictitems.service;

import org.psyduck.admin.dictitems.dao.DictItemsDao;
import org.psyduck.admin.dictitems.eneity.DictItems;
import org.psyduck.common.base.BaseService;
import org.springframework.stereotype.Service;

@Service
public class DictItemsService extends BaseService<DictItemsDao, DictItems> {
}