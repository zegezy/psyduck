package org.psyduck.admin.dictitems.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.psyduck.admin.dictitems.eneity.DictItems;

public interface DictItemsDao extends BaseMapper<DictItems> {

    @Select("select max(sort) + 10 from sys_dict_items where dict_id = #{dictId} and is_del = '0'")
    Integer getSort(String dictId);

}
