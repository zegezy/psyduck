package org.psyduck.admin.file.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.psyduck.common.base.BaseEntity;


@Data
@TableName("sys_file")
public class SysFile extends BaseEntity {

    /**
     * 文件路径
     */
    private String url;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 外部id，其他表数据id
     */
    private String externalId;

    /**
     * 外部文件类型，比如：是营业执照啊 还是 头像啊
     */
    private String externalType;

}