package org.psyduck.admin.file.service;

import org.psyduck.admin.file.dao.SysFileDao;
import org.psyduck.admin.file.entity.SysFile;
import org.psyduck.common.base.BaseService;
import org.springframework.stereotype.Service;

@Service
public class SysFileService extends BaseService<SysFileDao, SysFile> {

}