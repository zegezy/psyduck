package org.psyduck.admin.file.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.psyduck.admin.file.entity.SysFile;

public interface SysFileDao extends BaseMapper<SysFile> {

}