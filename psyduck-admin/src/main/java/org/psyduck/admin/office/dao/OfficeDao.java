package org.psyduck.admin.office.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.psyduck.admin.office.entity.Office;

public interface OfficeDao extends BaseMapper<Office> {

}