package org.psyduck.admin.office.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.psyduck.common.base.BaseEntity;


@Data
@TableName("sys_office")
public class Office extends BaseEntity {

    /**
     * 名称
     */
    private String name;

    /**
     * 类型
     */
    private String type;

    /**
     * 父id
     */
    private String pid;

    /**
     * 排序
     */
    private Integer sort;

}