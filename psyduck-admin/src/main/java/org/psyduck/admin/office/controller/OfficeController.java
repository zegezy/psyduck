package org.psyduck.admin.office.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.psyduck.admin.office.entity.Office;
import org.psyduck.admin.office.service.OfficeService;
import org.psyduck.common.model.PageJsonBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
@RequestMapping("office")
public class OfficeController {

    @Autowired
    OfficeService officeService;

    @PreAuthorize("hasAuthority('office:office:list')")
    @RequestMapping("list")
    public PageJsonBean list(Page<Office> page, Office office) {
        QueryWrapper queryWrapper = new QueryWrapper();
        return PageJsonBean.success(officeService.page(page,queryWrapper));
    }

    @PreAuthorize("hasAuthority('office:office:delete')")
    @RequestMapping("delete")
    public boolean delete(String... id){
        if(id.length > 1){
            return officeService.removeByIds(Arrays.asList(id));
        }
        return officeService.removeById(id[0]);
    }

    @PreAuthorize("hasAuthority('office:office:save')")
    @RequestMapping("save")
    public Boolean save(Office office) {
        return officeService.saveOrUpdate(office);
    }

}