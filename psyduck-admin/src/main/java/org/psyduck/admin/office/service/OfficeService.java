package org.psyduck.admin.office.service;

import org.psyduck.admin.office.dao.OfficeDao;
import org.psyduck.admin.office.entity.Office;
import org.psyduck.common.base.BaseService;
import org.springframework.stereotype.Service;

@Service
public class OfficeService extends BaseService<OfficeDao, Office> {

}