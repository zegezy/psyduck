package org.psyduck.admin.menu.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.psyduck.common.base.BaseEntity;

import java.util.ArrayList;
import java.util.List;

@TableName("sys_menu")
@Data
public class Menu extends BaseEntity implements Comparable<Menu> {

    String pid;

    String name;

    String url;

    String permission;

    Integer sort;

    /**
     * 描述
     */
    String descRibe;

    /**
     * 是否开启list配置：0开启，1不开启
     */
    Integer isListConfig;

    /**
     * 列表配置json
     */
    String listConfig;

    /**
     * 表单配置json
     */
    String formConfig;

    @TableField("is_show")
    Integer isShow;

    @TableField(exist = false)
    private List<Menu> children;

    public void addChild(Menu node) {
        if (this.children == null) {
            this.children = new ArrayList<>();
        }
        this.children.add(node);
    }

    public void sort() {
        if (this.children != null) {
            for (Menu node : children) {
                node.sort();
            }
        }
    }

    @Override
    public int compareTo(Menu o) {
        this.sort = this.sort == null ? 1 : this.sort;
        o.sort = o.sort == null ? 1 : o.sort;
        return this.sort.compareTo(o.sort);
    }

}
