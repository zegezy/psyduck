package org.psyduck.admin.menu.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.psyduck.admin.menu.entity.Menu;
import org.psyduck.common.entity.DataMap;

import java.util.List;

public interface MenuDao extends BaseMapper<Menu> {

    @Select("select distinct sm.* from sys_menu sm join sys_role_menu srm on sm.id = srm.menu_id join sys_user_role sur on sur.role_id = srm.role_id join sys_user su on su.id = sur.user_id and su.id = #{id} and su.is_del = 0 where sm.is_del = 0 and sm.is_show = 1 order by sort")
    List<Menu> getCurrentUserMenu(@Param("id") String id);

    @Select("select id from sys_menu where is_del = 0 and pid = #{pid}")
    List<String> selectMenusByPid(@Param("pid") String pid);

    @Select("select sm.id,sm.name from sys_menu sm join sys_role_menu srm on sm.id = srm.menu_id and srm.role_id = #{roleId} where sm.is_del = 0")
    List<DataMap> getMenusByRoleId(@Param("roleId") String roleId);

    @Select("select max(sort) + 10 from sys_menu where pid = #{pid} and is_del = '0'")
    Integer getSort(@Param("pid") String pid);

}
