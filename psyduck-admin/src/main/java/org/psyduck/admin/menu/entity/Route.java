package org.psyduck.admin.menu.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Route implements Comparable<Route> {

    private String id;

    private String pid;

    private String name;

    private String path;

    private String component;

    private String listConfig;

    private String formConfig;

    private Integer isListConfig;

    private Integer sort;

    private Meta meta;

    private Integer isShow;

    private boolean alwaysShow = false;

    private String redirect;

    @Data
    public static class Meta{

        private String title;

        private String icon;

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Route> children;

    public void addChild(Route node) {
        if (this.children == null) {
            this.children = new ArrayList<>();
        }
        this.children.add(node);
    }

    public void sort() {
        if (this.children != null) {
            for (Route node : children) {
                node.sort();
            }
        }
    }

    @Override
    public int compareTo(Route o) {
        this.sort = this.sort == null ? 1 : this.sort;
        o.sort = o.sort == null ? 1 : o.sort;
        return this.sort.compareTo(o.sort);
    }

}
