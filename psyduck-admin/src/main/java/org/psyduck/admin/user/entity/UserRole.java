package org.psyduck.admin.user.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("sys_user_role")
@Data
public class UserRole {

    private String userId;

    private String roleId;

}
