package org.psyduck.admin.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.psyduck.admin.user.entity.UserRole;

public interface UserRoleDao extends BaseMapper<UserRole> {

}