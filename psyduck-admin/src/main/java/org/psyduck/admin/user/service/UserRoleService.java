package org.psyduck.admin.user.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.psyduck.admin.user.dao.UserRoleDao;
import org.psyduck.admin.user.entity.UserRole;
import org.springframework.stereotype.Service;

@Service
public class UserRoleService extends ServiceImpl<UserRoleDao, UserRole> {

}