package org.psyduck.admin.role.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.psyduck.admin.role.dao.RoleMenuDao;
import org.psyduck.admin.role.eneity.RoleMenu;
import org.springframework.stereotype.Service;

@Service
public class RoleMenuService extends ServiceImpl<RoleMenuDao, RoleMenu> {
}