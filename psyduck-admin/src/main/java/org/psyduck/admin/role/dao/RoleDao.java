package org.psyduck.admin.role.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.psyduck.admin.role.eneity.Role;

public interface RoleDao extends BaseMapper<Role> {
}
