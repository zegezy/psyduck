package org.psyduck.admin.role.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.psyduck.admin.role.eneity.RoleMenu;

public interface RoleMenuDao extends BaseMapper<RoleMenu> {
}