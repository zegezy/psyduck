package org.psyduck.admin.role.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.psyduck.admin.role.eneity.Role;
import org.psyduck.admin.role.service.RoleService;
import org.psyduck.common.constant.Constants;
import org.psyduck.common.model.PageJsonBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("role")
public class RoleController {

    @Autowired
    RoleService roleService;

    @RequestMapping("getRoles")
    public List<Role> getRoles(){
        return roleService.list();
    }

    @RequestMapping("list")
    public PageJsonBean list(Page page,Role role){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc(Constants.BaseEntity.CREATE_DATE);
        queryWrapper.select("id","name");
        if(StrUtil.isNotBlank(role.getName())){
            queryWrapper.like("name",role.getName());
        }
        return PageJsonBean.success(roleService.page(page,queryWrapper));
    }

    @CacheEvict(value = "userAuth", allEntries=true)
    @PreAuthorize("hasAuthority('role:save')")
    @RequestMapping("save")
    public String save(Role role,String menus){
        return roleService.save(role,menus);
    }

    @CacheEvict(value = "userAuth", allEntries=true)
    @PreAuthorize("hasAuthority('role:delete')")
    @RequestMapping("delete")
    public Boolean delete(String id){
        return roleService.removeById(id);
    }

}