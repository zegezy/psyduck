package org.psyduck.admin.role.eneity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.psyduck.common.base.BaseEntity;

@Data
@TableName("sys_role")
public class Role extends BaseEntity {

    private String type;

    private String name;

    private Integer sort;

}
