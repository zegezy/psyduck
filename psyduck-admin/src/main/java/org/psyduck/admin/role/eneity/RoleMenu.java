package org.psyduck.admin.role.eneity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("sys_role_menu")
public class RoleMenu {

    private String roleId;

    private String menuId;

}
