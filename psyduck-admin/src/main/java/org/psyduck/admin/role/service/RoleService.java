package org.psyduck.admin.role.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.psyduck.admin.role.dao.RoleDao;
import org.psyduck.admin.role.eneity.Role;
import org.psyduck.admin.role.eneity.RoleMenu;
import org.psyduck.common.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleService extends BaseService<RoleDao, Role> {

    @Autowired
    RoleMenuService roleMenuService;

    @Transactional
    public String save(Role role,String menus){
        this.saveOrUpdate(role);
        roleMenuService.remove(new QueryWrapper<RoleMenu>().eq("role_id", role.getId()));
        String[] menuArr = menus.split(",");
        for (int i = 0; i < menuArr.length; i++) {
            String menu = menuArr[i];
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setMenuId(menu);
            roleMenu.setRoleId(role.getId());
            roleMenuService.save(roleMenu);
        }
        return role.getId();
    }

}