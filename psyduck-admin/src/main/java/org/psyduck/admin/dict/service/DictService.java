package org.psyduck.admin.dict.service;

import org.psyduck.admin.dict.dao.DictDao;
import org.psyduck.admin.dict.eneity.Dict;
import org.psyduck.common.base.BaseService;
import org.springframework.stereotype.Service;

@Service
public class DictService extends BaseService<DictDao, Dict> {
}