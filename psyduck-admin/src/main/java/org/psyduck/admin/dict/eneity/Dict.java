package org.psyduck.admin.dict.eneity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.psyduck.common.base.BaseEntity;

@Data
@TableName("sys_dict")
public class Dict extends BaseEntity {

    /**
     * 描述
     */
    private String descRibe;

    private String type;

    /**
     * 0 系统类
     * 1 业务类
     */
    private String dictType;

    private Integer sort;

    private String remarks;

}
