package org.psyduck.admin.dict.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.psyduck.admin.dict.eneity.Dict;
import org.psyduck.common.entity.DataMap;

import java.util.List;

public interface DictDao extends BaseMapper<Dict> {

    @Select("select label,value,d.type from sys_dict_items it left join sys_dict d on d.id = it.dict_id where it.is_del = '0' and d.is_del = '0' order by it.sort")
    List<DataMap> getDictItems();

    @Select("select max(sort) + 10 from sys_dict where is_del = '0'")
    Integer getSort();

}
