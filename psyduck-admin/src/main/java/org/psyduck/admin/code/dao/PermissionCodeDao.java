package org.psyduck.admin.code.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.psyduck.admin.code.entity.PermissionCode;

public interface PermissionCodeDao extends BaseMapper<PermissionCode> {

}