package org.psyduck.admin.code.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.psyduck.common.base.BaseEntity;


@Data
@TableName("sys_permission_code")
public class PermissionCode extends BaseEntity {

    
    
    /**
     * 权限代码
     */
    private String code;
    
    /**
     * 父id
     */
    private String pid;
    

}