package org.psyduck.admin.code.service;

import org.psyduck.admin.code.dao.PermissionCodeDao;
import org.psyduck.admin.code.entity.PermissionCode;
import org.psyduck.common.base.BaseService;
import org.springframework.stereotype.Service;

@Service
public class PermissionCodeService extends BaseService<PermissionCodeDao, PermissionCode> {

}