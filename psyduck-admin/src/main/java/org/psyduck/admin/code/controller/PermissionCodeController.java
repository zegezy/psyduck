package org.psyduck.admin.code.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.psyduck.admin.code.entity.PermissionCode;
import org.psyduck.admin.code.service.PermissionCodeService;
import org.psyduck.common.model.PageJsonBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("permissionCode")
public class PermissionCodeController {

    @Autowired
    PermissionCodeService permissionCodeService;

    @PreAuthorize("hasAuthority('code:permissionCode:list')")
    @RequestMapping
    public PageJsonBean list(Page<PermissionCode> page, PermissionCode permissionCode) {
        QueryWrapper queryWrapper = new QueryWrapper();
        return PageJsonBean.success(permissionCodeService.page(page,queryWrapper));
    }

    @PreAuthorize("hasAuthority('code:permissionCode:delete')")
    @RequestMapping("delete")
    public Boolean delete(String id) {
        return permissionCodeService.removeById(id);
    }

    @PreAuthorize("hasAuthority('code:permissionCode:save')")
    @RequestMapping("save")
    public Boolean save(PermissionCode permissionCode) {
        return permissionCodeService.saveOrUpdate(permissionCode);
    }

}