package org.psyduck.gen.service;

import freemarker.template.Configuration;
import freemarker.template.Template;
import org.psyduck.common.utils.Global;
import org.psyduck.gen.dao.GenDao;
import org.psyduck.gen.entity.TableColumns;
import org.psyduck.gen.utils.GenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GenService {

    @Autowired
    GenDao genDao;

    public void genEntity(String packageName,String moduleName,String entityName,String tableName) {
        List<TableColumns> tableColumns = genDao.selectTable(tableName);
        boolean importDate = false;
        for (int i = 0; i < tableColumns.size(); i++) {
            tableColumns.get(i).setDataType(GenUtils.dataTypeConver(tableColumns.get(i).getDataType()));
            tableColumns.get(i).setColumnName(GenUtils.lineToHump(tableColumns.get(i).getColumnName()));
            if(tableColumns.get(i).getDataType().equals("Date")){
                importDate = true;
            }
        }
        Map<String,Object> variables = new HashMap<>();
        variables.put("tableColumns",tableColumns);
        variables.put("entityName",entityName);
        variables.put("moduleName",moduleName);
        variables.put("packageName",packageName);
        variables.put("importDate",importDate);
        variables.put("tableName",tableName);
        templateProcess(variables,"entity","org/psyduck/"+packageName+"/"+moduleName+"/entity/",entityName+".java");
    }

    public void genDao(String packageName,String moduleName,String entityName){
        Map<String,Object> variables = new HashMap<>();
        variables.put("entityName",entityName);
        variables.put("moduleName",moduleName);
        variables.put("packageName",packageName);
        templateProcess(variables,"dao","org/psyduck/"+packageName+"/"+moduleName+"/dao/",entityName+"Dao.java");
    }

    public void genService(String packageName,String moduleName,String entityName){
        Map<String,Object> variables = new HashMap<>();
        variables.put("entityName",entityName);
        variables.put("moduleName",moduleName);
        variables.put("packageName",packageName);
        templateProcess(variables,"service","org/psyduck/"+packageName+"/"+moduleName+"/service/",entityName+"Service.java");
    }

    /**
     * 首字母转小写
     * @param s
     * @return
     */
    public static String toLowerCaseFirstOne(String s) {
        if (Character.isLowerCase(s.charAt(0))) {
            return s;
        } else {
            return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
        }
    }

    public void genController(String packageName,String moduleName,String entityName){
        Map<String,Object> variables = new HashMap<>();
        variables.put("entityName",entityName);
        variables.put("moduleName",moduleName);
        variables.put("packageName",packageName);
        variables.put("entityNameFirstLower", toLowerCaseFirstOne(entityName));
        templateProcess(variables,"controller","org/psyduck/"+packageName+"/"+moduleName+"/controller/",entityName+"Controller.java");
    }

    public void genJava(String packageName,String moduleName,String entityName,String tableName){
        genEntity(packageName, moduleName, entityName, tableName);
        genDao(packageName, moduleName, entityName);
        genService(packageName, moduleName, entityName);
        genController(packageName, moduleName, entityName);
        openDir(packageName);
    }

    public void openDir(String packageName) {
        try {
            String[] cmd = new String[5];
            cmd[0] = "cmd";
            cmd[1] = "/c";
            cmd[2] = "start";
            cmd[3] = " ";
            cmd[4] = Global.genPath + "org/psyduck";
            System.out.println(cmd);
            Runtime.getRuntime().exec(cmd);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void templateProcess(Map<String,Object> variables,String ftlName,String packageName,String fileName){
        try {
            File file = new File(Global.genPath + packageName);
            if(!file.isDirectory()){
                file.mkdirs();
            }
            //创建配置实例
            Configuration configuration = new Configuration();
            //设置编码
            configuration.setDefaultEncoding("UTF-8");
            //ftl模板文件
            configuration.setClassForTemplateLoading(GenService.class,"/ftl");
            //获取模板
            Template template = configuration.getTemplate(ftlName + ".ftl");
            //渲染模板
            FileWriter write = new FileWriter(Global.genPath + packageName + fileName);
            template.process(variables, write);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}