package org.psyduck.gen.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.psyduck.gen.entity.TableColumns;

import java.util.List;

public interface GenDao extends BaseMapper {

    @Select("SELECT column_name,column_comment,data_type FROM information_schema.COLUMNS WHERE table_name = #{tableName} AND column_name NOT IN ('id','is_del','create_by','create_date','update_by','update_date') and table_schema = database()")
    List<TableColumns> selectTable(@Param("tableName") String tableName);

}