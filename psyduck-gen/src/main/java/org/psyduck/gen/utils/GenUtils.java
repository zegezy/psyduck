package org.psyduck.gen.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GenUtils {

    private static Pattern linePattern = Pattern.compile("_(\\w)");

    public static String lineToHump(String str) {
		str = str.toLowerCase();
		Matcher matcher = linePattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	public static String dataTypeConver(String dataType){
		return dataType.equals("varchar") ? "String" :
				dataType.equals("char") ? "String" :
				dataType.equals("int") ? "Integer" :
				dataType.equals("datetime")  || dataType.equals("date")? "Date" :
				dataType.equals("integer") ? "Integer" :
				dataType.equals("text") ? "String" :
				dataType.equals("longtext") ? "String" :
				dataType.equals("double") ? "Double" : "";
	}

}
