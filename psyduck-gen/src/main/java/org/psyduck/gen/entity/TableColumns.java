package org.psyduck.gen.entity;

public class TableColumns {

    /**
     * 列名
     */
    private String columnName;

    /**
     * 注释
     */
    private String columnComment;

    /**
     * 数据类型
     */
    private String dataType;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnComment() {
        return columnComment;
    }

    public void setColumnComment(String columnComment) {
        this.columnComment = columnComment;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

}
