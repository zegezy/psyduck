import request from '@/scripts/request'

export function getRoles() {
  return request({
    url: 'role/getRoles',
    method: 'get'
  })
}

export function saveData(params) {
  return request({
    url: 'role/save',
    method: 'post',
    params
  })
}
