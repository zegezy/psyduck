import Vue from 'vue'
import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/scripts/auth' // get token from cookie
import getPageTitle from '@/scripts/get-page-title'
import axios from 'axios'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ['/login'] // no redirect whitelist

router.beforeEach(async(to, from, next) => {
  // start progress bar
  NProgress.start()

  // set page title
  document.title = getPageTitle(to.meta.title)

  // determine whether the user has logged in
  const hasToken = getToken()

  if (hasToken) {
    var tokenIsOverdue
    await axios({
      baseURL: process.env.VUE_APP_BASE_API,
      url: 'security/validateToken',
      params: {
        token: hasToken
      }
    }).then(res => {
      const { data } = res
      tokenIsOverdue = data.data
    })
    if (to.path !== '/login' && !tokenIsOverdue) {
      store.dispatch('user/logout').then(() => {
        next(`/login`)
        NProgress.done()
      })
    } else {
      if (to.path === '/login') {
        // if is logged in, redirect to the home page
        next({ path: '/' })
        NProgress.done()
      } else {
        // determine whether the user has obtained his permission roles through getInfo
        if (Vue.prototype.$authorities) {
          next()
        } else {
          try {
            // get user info
            // note: roles must be a object array! such as: ['admin'] or ,['developer','editor']
            await store.dispatch('user/getUserInfo')
            await Vue.prototype.$common.getDictData()
            await Vue.prototype.$common.loadConfig()
            // generate accessible routes map based on roles
            const accessRoutes = await store.dispatch('permission/generateRoutes')
            // dynamically add accessible routes
            router.addRoutes(accessRoutes)
            // hack method to ensure that addRoutes is complete
            // set the replace: true, so the navigation will not leave a history record
            next({ ...to, replace: true })
          } catch (error) {
            console.log(error)
            // remove token and go to login page to re-login
            await store.dispatch('user/resetToken')
            Message.error(error.data || 'Has Error')
            next(`/login`)
            NProgress.done()
          }
        }
      }
    }
  } else {
    /* has no token*/

    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next()
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})

