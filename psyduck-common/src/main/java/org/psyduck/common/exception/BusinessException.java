package org.psyduck.common.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.psyduck.common.model.JsonCode;

@Getter
@AllArgsConstructor
public class BusinessException extends RuntimeException{

    private JsonCode jsonCode;

    private String message;

}
