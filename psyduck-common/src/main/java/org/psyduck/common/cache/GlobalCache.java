package org.psyduck.common.cache;

import cn.hutool.cache.Cache;
import cn.hutool.cache.CacheUtil;
import org.springframework.stereotype.Component;

@Component
public class GlobalCache {

    Cache<String, String> lfuCache = CacheUtil.newLFUCache(300);

    public void put(String key, String value) {
        lfuCache.put(key, value);
    }

    public String get(String key) {
        return lfuCache.get(key);
    }

    public void remove(String key) {
        lfuCache.remove(key);
    }

}
