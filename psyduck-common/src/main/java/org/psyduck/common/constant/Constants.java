package org.psyduck.common.constant;

public class Constants {

    public static final String DELETE = "1";

    public static final String NOT_DELETE = "0";

    public static final String IS_DEL = "is_del";

    public static class User{

        public static final String SUPER_ADMIN_ID = "1";

    }

    public static class BaseEntity{

        public static final String CREATE_DATE = "create_date";

    }

    public static class Menu{

        /**
         * menu表 - 功能菜单id
         */
        public static final String FUNCTION_MENU_ID = "82de38c53fbe4c4185e31d972d8c2d5e";

    }

}
