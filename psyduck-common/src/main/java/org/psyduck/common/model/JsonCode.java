package org.psyduck.common.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum JsonCode {

    SUCCESS(200, "成功"),
    BAD_REQUEST(400, "参数或者语法不对"),
    CERTIFICATE_EXPIRED(402, "凭证已过期"),
    LOGIN_ERROR(401, "登陆失败，用户名或密码无效"),
    FORBIDDEN(403, "禁止访问"),
    NOT_FOUND(404, "请求的资源不存在"),
    OPERATE_ERROR(405, "操作失败，请求操作的资源不存在"),
    TIME_OUT(408, "请求超时"),
    SERVER_ERROR(500, "服务器内部错误"),

    NOT_FOUND_USER(1, "没有找到用户"),

    JWT_GET_USER_TOKEN_ERROR(2,"根据token获取用户异常"),
    JWT_GET_CLAIMS_ERROR(3,"获取claims异常"),
    JWT_GET_USER_ID_ERROR(4,"获取userId异常"),
    JWT_GET_USER_NAME_ERROR(5,"获取username异常"),

    USER_LOCK(10001,"用户已被锁定")

    ;

    private int code;

    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
