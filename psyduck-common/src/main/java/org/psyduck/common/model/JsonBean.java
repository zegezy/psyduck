package org.psyduck.common.model;

import lombok.Getter;

/**
 * 统一返回值对象
 */
@Getter
public class JsonBean {

    /**
     * 状态码
     */
    private int code = 200;

    /**
     * 状态说明
     */
    private String message = "success";

    /**
     * 实际逻辑
     */
    private Object data;

    /**
     * 服务器时间
     */
    private long timestamp = System.currentTimeMillis();

    public static JsonBean success(){
        return new JsonBean();
    }

    public static JsonBean success(Object data){
        return new JsonBean(data);
    }

    public static JsonBean exception(JsonCode jsonCode,Object data){
        return new JsonBean(jsonCode,data);
    }

    public static JsonBean notLogin(){
        return new JsonBean(JsonCode.CERTIFICATE_EXPIRED,null);
    }

    public static JsonBean permissionDenied(){
        return new JsonBean(JsonCode.FORBIDDEN,null);
    }

    public JsonBean(JsonCode jsonCode,Object data) {
        this.code = jsonCode.getCode();
        this.message = jsonCode.getMessage();
        this.data = data;
    }

    public JsonBean(Object data) {
        this.data = data;
    }

    public JsonBean(JsonCode jsonCode) {
        this.code = jsonCode.getCode();
        this.message = jsonCode.getMessage();
    }

    public JsonBean(){}

}