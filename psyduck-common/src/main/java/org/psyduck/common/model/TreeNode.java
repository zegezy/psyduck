package org.psyduck.common.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TreeNode<T extends Comparable<T>> implements Comparable<TreeNode<T>> {

    private T node;

    private List<TreeNode<T>> children;

    public TreeNode() {
        super();
    }

    public TreeNode(T node) {
        super();
        this.node = node;
    }

    public T getNode() {
        return node;
    }

    public void setNode(T node) {
        this.node = node;
    }

    public List<TreeNode<T>> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode<T>> children) {
        this.children = children;
    }

    public void addChild(TreeNode<T> node) {
        if (this.children == null) {
            this.children = new ArrayList<>();
        }
        this.children.add(node);
    }

    public void sort() {
        if (this.children != null) {
            Collections.sort(this.children);
            for (TreeNode<T> node : children) {
                node.sort();
            }
        }
    }

    @Override
    public int compareTo(TreeNode<T> o) {
        return node.compareTo(o.node);
    }
}
