package org.psyduck.common.model;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Getter;

@Getter
public class PageJsonBean extends JsonBean{
	
	private long total;

	public PageJsonBean(IPage page){
		super(page.getRecords());
		this.total = page.getTotal();
	}

	public static PageJsonBean success(IPage page){
		return new PageJsonBean(page);
	}

}