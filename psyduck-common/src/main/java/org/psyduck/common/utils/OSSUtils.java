package org.psyduck.common.utils;

import java.io.InputStream;
import java.util.Map;

public interface OSSUtils {

    Map<String, String> uploadFileToOss(String originalFilename, byte[] bytes, InputStream inputStream);

    void deleteFile(String key);

    default boolean isDefault(){
        return true;
    }

}
