package org.psyduck.common.utils;

import cn.hutool.core.io.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FileOperation {

    @Autowired
    OSSUtils ossUtils;

    public void fileDelete(String url){
        if(Global.UPLOAD_TYPE.equals("oss") && !ossUtils.isDefault()){
            ossUtils.deleteFile(url);
        }else{
            String realPath = Global.getUserFilesBaseDir() + "/" + url;
            FileUtil.del(realPath);
        }
    }

}
