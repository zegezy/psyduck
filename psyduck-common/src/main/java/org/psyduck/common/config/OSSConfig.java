package org.psyduck.common.config;

import org.psyduck.common.impl.DefaultOSSImpl;
import org.psyduck.common.utils.OSSUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OSSConfig {

    @Bean
    @ConditionalOnMissingBean(OSSUtils.class)
    public OSSUtils ossUtils(){
        return new DefaultOSSImpl();
    }

}
