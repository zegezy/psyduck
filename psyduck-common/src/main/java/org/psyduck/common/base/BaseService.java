package org.psyduck.common.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseService<D, T extends BaseEntity> extends ServiceImpl<BaseMapper<T>, T> {

    @Autowired
    protected D dao;

    public D getDao() {
        return dao;
    }

    @Override
    public boolean saveOrUpdate(T entity) {
        if (entity.getId() != null) {
            entity.preUpdate();
        } else {
            entity.preInsert();
        }
        return super.saveOrUpdate(entity);
    }

    @Override
    public boolean save(T entity) {
        entity.preInsert();
        return super.save(entity);
    }

    @Override
    public boolean updateById(T entity) {
        entity.preUpdate();
        return super.updateById(entity);
    }

}