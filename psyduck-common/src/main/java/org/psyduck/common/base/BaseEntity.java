package org.psyduck.common.base;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import org.psyduck.common.utils.WebUtils;

import java.util.Date;

@Data
public class BaseEntity {

    @TableId(type = IdType.ASSIGN_UUID)
    public String id;

    @TableLogic
    @TableField("is_del")
    public Integer isDel;

    public String createBy;

    public Date createDate;

    public String updateBy;

    public Date updateDate;

    @TableField(exist = false)
    public String orderBy;

    public void preInsert() {
        String userId = WebUtils.getUserId();
        if (userId != null) {
            this.updateBy = userId;
            this.createBy = userId;
        }
        this.updateDate = new Date();
        this.createDate = this.updateDate;
    }

    public void preUpdate() {
        String userId = WebUtils.getUserId();
        if (userId != null) {
            this.updateBy = userId;
        }
        this.updateDate = new Date();
    }

}