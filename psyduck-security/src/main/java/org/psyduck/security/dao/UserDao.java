package org.psyduck.security.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.psyduck.security.model.User;

import java.util.List;

public interface UserDao extends BaseMapper<User> {

    @Select("select * from sys_user where is_del = 0 and username = #{username}")
    User getUserByUsername(@Param("username") String username);

    IPage<User> selectPage(Page page, User user);

    @Select("select role_id from sys_user_role where user_id = #{userId}")
    List<String> getUserRoles(String userId);

}