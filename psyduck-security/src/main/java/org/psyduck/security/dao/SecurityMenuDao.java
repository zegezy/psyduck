package org.psyduck.security.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.psyduck.common.entity.DataMap;

import java.util.List;

public interface SecurityMenuDao extends BaseMapper<DataMap> {

    @Select("SELECT DISTINCT\n" +
            "            sm.permission\n" +
            "        FROM\n" +
            "            sys_menu sm\n" +
            "            JOIN sys_role_menu srm ON sm.id = srm.menu_id\n" +
            "            JOIN sys_user_role sur ON sur.role_id = srm.role_id\n" +
            "            JOIN sys_user su ON su.id = sur.user_id\n" +
            "            AND su.id = #{id}\n" +
            "            AND su.is_del = 0\n" +
            "        WHERE\n" +
            "            sm.is_del = 0\n" +
            "            AND sm.permission != ''\n" +
            "            AND sm.permission IS NOT NULL")
    List<String> getCurrentUserPermissions(@Param("id") String id);

    @Select("SELECT DISTINCT\n" +
            "            sm.permission\n" +
            "        FROM\n" +
            "            sys_menu sm\n" +
            "        WHERE\n" +
            "            sm.is_del = 0\n" +
            "            AND sm.permission != ''\n" +
            "            AND sm.permission IS NOT NULL")
    List<String> getAllPermissions();

}
