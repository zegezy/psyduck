package org.psyduck.security.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.psyduck.common.base.BaseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

@TableName("sys_user")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User extends BaseEntity implements UserDetails {

    private String id;

    /**
     * 登录名
     */
    private String username;

    /**
     * 姓名/昵称
     */
    private String name;

    /**
     * 密码
     */
    private String password;

    /**
     * 目前没有用
     */
    private String status;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 禁止登录：0未禁用，1已禁用
     */
    private Integer isLogin;

    /**
     * 用户角色列表
     */
    @TableField(exist = false)
    private List<String> roles;

    /**
     * 排序
     */
    private Integer sort;

    @TableField(exist = false)
    public List<GrantedAuthority> authorities;

    //返回分配给用户的权限标识列表
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public boolean hasAuthority(String authority){
        return this.authorities.stream().anyMatch(it -> it.getAuthority().equals(authority));
    }

    /**
     * 账户是否未过期
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     *  账户是否未锁定
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    /**
     * 密码是否未过期
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 账户是否激活
     */
    @Override
    public boolean isEnabled() {
        return true;
    }

}