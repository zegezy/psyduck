package org.psyduck.security.service;

import org.psyduck.security.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * 登陆身份认证
 */
@Component
public class SecurityUserDetailsService implements UserDetailsService {

    @Autowired
    SecurityUserService userService;

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = (User) userService.getDao().getUserByUsername(username);
        if(null != user){
            user.setAuthorities(userService.getCurrentUserAuthorities(user.getId()));
        }
        return user;
    }

}