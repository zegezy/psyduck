package org.psyduck.gen;

import cn.hutool.db.nosql.redis.RedisDS;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.psyduck.PsyDuckApplication;
import org.psyduck.gen.service.GenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsyDuckApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration(exclude = {
    SecurityAutoConfiguration.class
})
public class GenTest {

    @Autowired
    GenService genService;

    @Test
    public void main(){
        genService.genJava("admin","office","Office","sys_office");
    }

    @Test
    public void redisTest() {
        RedisDS.create().setStr("666","777");
        System.out.println("设置之后："+RedisDS.create().getStr("666"));
        RedisDS.create().del("666");
        System.out.println("删除之后："+RedisDS.create().getStr("666"));
    }

}