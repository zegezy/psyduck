package org.psyduck.controller.common;

import org.psyduck.common.utils.WebUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RequestMapping("config")
@RestController
public class ConfigController {

    @RequestMapping("list")
    public Map<String, String> list(){
        Map<String, String> map = new HashMap<>();
        map.put("filePrefix", WebUtils.getUeditorPrefix());
        return map;
    }

}
