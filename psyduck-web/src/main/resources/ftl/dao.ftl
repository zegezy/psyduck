package org.psyduck.${packageName}.${moduleName}.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.psyduck.${packageName}.${moduleName}.entity.${entityName};

public interface ${entityName}Dao extends BaseMapper<${entityName}> {

}