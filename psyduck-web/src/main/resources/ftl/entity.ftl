package org.psyduck.${packageName}.${moduleName}.entity;

import org.psyduck.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
<#if importDate>import java.util.Date;</#if>

@Data
@TableName("${tableName}")
public class ${entityName} extends BaseEntity {

    <#list tableColumns as column>
    <#if column.columnComment != ''>
    /**
     * ${column.columnComment}
     */
    </#if>
    private ${column.dataType} ${column.columnName};
    <#if column_has_next>

    <#else >
    </#if>
    </#list>

}