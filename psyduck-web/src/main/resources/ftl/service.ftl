package org.psyduck.${packageName}.${moduleName}.service;

import org.springframework.stereotype.Service;
import org.psyduck.common.base.BaseService;
import org.psyduck.${packageName}.${moduleName}.dao.${entityName}Dao;
import org.psyduck.${packageName}.${moduleName}.entity.${entityName};

@Service
public class ${entityName}Service extends BaseService<${entityName}Dao, ${entityName}> {

}