package org.psyduck.${packageName}.${moduleName}.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.psyduck.${packageName}.${moduleName}.entity.${entityName};
import org.psyduck.${packageName}.${moduleName}.service.${entityName}Service;
import cn.hutool.core.util.StrUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.psyduck.common.model.PageJsonBean;
import java.util.Arrays;

@RestController
@RequestMapping("${entityNameFirstLower}")
public class ${entityName}Controller {

    @Autowired
    ${entityName}Service ${entityNameFirstLower}Service;

    @PreAuthorize("hasAuthority('${moduleName}:${entityNameFirstLower}:list')")
    @RequestMapping("list")
    public PageJsonBean list(Page<${entityName}> page, ${entityName} ${entityNameFirstLower}) {
        QueryWrapper queryWrapper = new QueryWrapper();
        return PageJsonBean.success(${entityNameFirstLower}Service.page(page,queryWrapper));
    }

    @PreAuthorize("hasAuthority('${moduleName}:${entityNameFirstLower}:delete')")
    @RequestMapping("delete")
    public boolean delete(String... id){
        if(id.length > 1){
            return ${entityNameFirstLower}Service.removeByIds(Arrays.asList(id));
        }
        return ${entityNameFirstLower}Service.removeById(id[0]);
    }

    @PreAuthorize("hasAuthority('${moduleName}:${entityNameFirstLower}:save')")
    @RequestMapping("save")
    public Boolean save(${entityName} ${entityNameFirstLower}) {
        return ${entityNameFirstLower}Service.saveOrUpdate(${entityNameFirstLower});
    }

}